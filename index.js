console.log('Welcome to JS!');
console.log('This is my work for today! 👻');

function changeColor() {
  const text = document.querySelector('#main-text');

  if (text.className !== 'blue-color') {
    text.className = 'blue-color';
  } else {
    text.className = 'red-color';
  }
}
